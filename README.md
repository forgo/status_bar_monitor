<!--
 * @Author: hua
 * @Date: 2019-01-24 11:18:00
 * @LastEditors: hua
 * @LastEditTime: 2019-05-21 09:48:31
 -->
# 状态栏监听

#### 介绍
基于dcloud h5+技术离线打包的状态栏监听的安卓客户端

#### 安卓体验下载
![状态栏监听](https://images.gitee.com/uploads/images/2019/0124/112908_a8e3fe2f_1588193.png "1548300239.png")


#### 软件架构
软件架构说明

1. 使用vue作为核心框架
2. 使用ydui作为ui框架
3. 使用plus+调用物理机api（dcloud的方案，http://www.html5plus.org/doc/zh_cn/accelerometer.html）


#### 安装教程


#### 使用说明

1. 安装
2. 同意监听权限
3. 填写回调地址

#### 使用截图
 ![输入图片说明](https://images.gitee.com/uploads/images/2019/0124/115839_dcbe3700_1588193.jpeg "Screenshot_20190124-112048.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0124/115847_9724aea8_1588193.jpeg "Screenshot_20190124-112129.jpg")

#### 作者其他开源产品
1. <a href="https://gitee.com/huashiyuting/flask " target="_blank">mvc分层,json api载体(中庸)的flask</a>
2. <a href="https://gitee.com/huashiyuting/tool_chicken" target="_blank">工具鸡前端app项目 </a>
3. <a href="https://gitee.com/huashiyuting/plainCms" target="_blank">plainCms</a>
4. <a href="https://gitee.com/huashiyuting/chat" target="_blank">一个基于socketio开发的简易聊天室，功能精简，易扩展。 </a>

#### 捐助作者
![捐助作者](https://images.gitee.com/uploads/images/2019/0124/105407_661d1190_1588193.png "mm_facetoface_collect_qrcode_1548297043215.png")
